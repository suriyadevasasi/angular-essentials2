import { Component, OnInit } from '@angular/core';

@Component({
 // selector: '[app-servers]',
  selector:'./app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'No Server was Created';
  serverName="Test Server";
  serverCreated = false;
  showText = false;
  servers =['TestServer', 'TestServer2']
  buttonClicks = [];
  
  
  constructor() 
  {
    setTimeout(() => { this.allowNewServer = true; } ,2000);

  }

  ngOnInit() {
  }

  onCreatedServer(){
    this.serverCreated = true;
    this.servers.push(this.serverName)
    this.serverCreationStatus="Server was created with the name :" + this.serverName; 

  }

  onUpdateServerName(event: any){
    this.serverName = (<HTMLInputElement>event.target).value;

  }
  
  onToggleDetails(){
    this.showText= !this.showText;
    //this.buttonClicks.push(this.buttonClicks.length + 1 );
   this.buttonClicks.push(new Date());
  }
 
  }


